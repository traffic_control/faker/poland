<?php

namespace Faker\Test\Poland;

use Faker\Generator;
use Faker\Poland\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    protected function setUp(): void
    {
        $faker = new Generator();
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    /**
     * Test the validity of state
     */
    public function testState()
    {
        $state = $this->_faker->state();
        $this->assertNotEmpty($state);
        $this->matchesRegularExpression('/[a-z]+/', $state);
    }
}
